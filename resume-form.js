import React, { useState } from 'react';
import {
    TextField,
    Button,
    Box,
    Typography,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    IconButton,
} from '@material-ui/core';
import { AddCircleOutline, RemoveCircleOutline } from '@material-ui/icons';

const ResumeForm = () => {
    const [resume, setResume] = useState({
        personalInformation: {
            fullName: '',
            email: '',
            phone: '',
            address: '',
            linkedInProfile: '',
            website: '',
        },
        objective: '',
        skills: [''],
        languages: [
            {
                language: '',
                proficiency: '',
            },
        ],
        // Add the initial state for the other properties of the schema here
    });

    const handleInputChange = (e, field, index = null) => {
        const { name, value } = e.target;
        let updatedResume = { ...resume };
        if (index !== null) {
            updatedResume[field][index][name] = value;
        } else {
            updatedResume[field][name] = value;
        }
        setResume(updatedResume);
    };

    const addSkill = () => {
        setResume({ ...resume, skills: [...resume.skills, ''] });
    };

    const removeSkill = (index) => {
        const newSkills = resume.skills.filter((_, i) => i !== index);
        setResume({ ...resume, skills: newSkills });
    };

    const addLanguage = () => {
        setResume({
            ...resume,
            languages: [...resume.languages, { language: '', proficiency: '' }],
        });
    };

    const removeLanguage = (index) => {
        const newLanguages = resume.languages.filter((_, i) => i !== index);
        setResume({ ...resume, languages: newLanguages });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(resume);
    };

    return (

        <Box>
            <form onSubmit={handleSubmit} >
                <Box>
                    <Typography variant="h6" > Personal Information </Typography>
                    < Grid container spacing={2} >
                        {/* Full Name */}
                        < Grid item xs={12} sm={6} >
                            <TextField
                                fullWidth
                                label="Full Name"
                                name="fullName"
                                value={resume.personalInformation.fullName}
                                onChange={(e) => handleInputChange(e, 'personalInformation')}
                            />
                        </Grid>
                        {/* Email */}
                        <Grid item xs={12} sm={6} >
                            <TextField
                                fullWidth
                                label="Email"
                                name="email"
                                value={resume.personalInformation.email}
                                onChange={(e) => handleInputChange(e, 'personalInformation')}
                            />
                        </Grid>
                        {/* Add other fields for personal information */}
                    </Grid>
                </Box>

                < Box >
                    <Typography variant="h6" > Objective </Typography>
                    < TextField
                        fullWidth
                        label="Objective"
                        name="objective"
                        value={resume.objective}
                        onChange={(e) => handleInputChange(e, 'objective')}
                        multiline
                        rows={4}
                    />
                </Box>

                < Box >
                    <Typography variant="h6" > Skills </Typography>
                    {
                        resume.skills.map((skill, index) => (
                            <Box key={index} >
                                <TextField
                                    label="Skill"
                                    name="skill"
                                    value={skill}
                                    onChange={(e) => handleInputChange(e, 'skills', index)}
                                />
                                < IconButton onClick={() => removeSkill(index)}>
                                    <RemoveCircleOutline />
                                </IconButton>
                            </Box>
                        ))}
                    <IconButton onClick={addSkill}>
                        <AddCircleOutline />
                    </IconButton>
                </Box>

                < Box >
                    <Typography variant="h6" > Languages </Typography>
                    {
                        resume.languages.map((language, index) => (
                            <Box key={index} >
                                <TextField
                                    label="Language"
                                    name="language"
                                    value={language.language}
                                    onChange={(e) => handleInputChange(e, 'languages', index)}
                                />
                                < TextField
                                    label="Proficiency"
                                    name="proficiency"
                                    value={language.proficiency}
                                    onChange={(e) => handleInputChange(e, 'languages', index)}
                                />
                                < IconButton onClick={() => removeLanguage(index)}>
                                    <RemoveCircleOutline />
                                </IconButton>
                            </Box>
                        ))}
                    <IconButton onClick={addLanguage}>
                        <AddCircleOutline />
                    </IconButton>
                </Box>

                {/* Add form fields for the other sections of the JSON schema here */}

                <Box mt={2}>
                    <Button type="submit" variant="contained" color="primary" >
                        Submit
                    </Button>
                </Box>
            </form>
        </Box>
    )
};
